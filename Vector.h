#pragma once
class Vector
{
private:
	double x;
	double y;
	double z;

public:
	Vector();
	Vector(double _x, double _y, double _z);
	void print();
	double getModule();
};