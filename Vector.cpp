#include "Vector.h"
#include <iostream>
#include <cmath>

Vector::Vector() : x(0), y(0), z(0)
{
}

Vector::Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
{
}

void Vector::print()
{
	std::cout << "x: " << x << " y: " << y << " z:" << z << std::endl;
}

double Vector::getModule()
{
	return sqrt(pow(x, 2) + pow(y, 2) + pow(z, 2));
}