﻿#include <iostream>
#include "Vector.h"

int main()
{
    Vector vec1;
    Vector vec2(3, 4, 5);

    vec1.print();
    std::cout << "The modulus of the vector is " << vec1.getModule() << std::endl;
    vec2.print();
    std::cout << "The modulus of the vector is " << vec2.getModule() << std::endl;
}